# Docker setup instruction

This is a template for simple web page. Some adjustments needed, before using `docker-compose build`.

## Step 1 (optional)

Create volume for DB.

**Before running command below, set desired path and custom volume name.**

`docker volume create --opt type=none --opt device=/srv/volume/volume_db --opt o=bind volume_db`


## Step 2

Edit `./docker-compose.yml`.

1. Set volume name. Replace `volume_db` (2 occurrences) with Your own name (see step 1).
2. In apache service, adjust files directory (1 occurrence).


## Step 3

Edit `./docker/resources/vhost.conf`.

Update `/srv/www/host.local/public` (4 occurrences) with Your own directory.


## Additional notes

There's missing full support for HTTPS connection.
